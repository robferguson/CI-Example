all: CI-Test

CI-Test: CI-Test.o dlist.o
	g++ -o CI-Test CI-Test.o dlist.o

CI-Test.o: CI-Test.cpp dlist.h
	g++ -c CI-Test.cpp

dlist.o: dlist.cpp 
	g++ -c dlist.cpp

clean:
	rm -f CI-Test.o dlist.o


