# GitLab Continuous Integration Example

This repository exists to test some of the basic functionality of the [continuous integration (CI) service](https://about.gitlab.com/gitlab-ci/) provided by GitLab.


## General Information

Each push to the remote master repository will trigger a continuous integration pipeline if two steps are followed:
1. A ['gitlab-ci.yml'](https://docs.gitlab.com/ce/ci/quick_start/#quick-start) file is added to the repository
2. A [GitLab Runner machine has been configured to work with the project](https://docs.gitlab.com/ce/ci/runners/README.html#runners)


The YAML file provides instructions (called _jobs_) that are executed by the GitLab Runner. These jobs are divided into stages of execution.
Generally, the stages occur in some sequential order, and if a certain stage fails, the following stages will not be executed.
More information about how to configure a general '.gitlab-ci.yml' file can be found [here.](https://docs.gitlab.com/ce/ci/yaml/).

A GitLab Runner is required for the CI pipeline to work. Information about Runners can be found [here.](https://docs.gitlab.com/ce/ci/runners/README.html#runners)
GitLab also provides public access to a number of shared virtual machine Runners that can be utilized in conjunction with the CI service, which is the method used for this project.


## CI Pipelines
GitLab's CI pipelines are the instruction sequences to be automatically executed whenever new content is pushed to the remote master branch of the project repository. If the CI services are successfully configured (a .gitlab-ci.yml file has been added and a GitLab Runner has been set up), pipeline information can be viewed from the `/pipelines` page of the project webpage on GitLab.

Whenever someone pushes to the remote master branch, a pipeline is initiated, and its status should say "running" on the `/pipelines` page. At this point, the virtual machines are being created for the stages and the specified jobs are being executed. The status of each stage within a pipeline can be viewed here as well. If a single stage fails, subsequent stages are not executed and the pipeline status should say "failed". If all stages pass, the status should be listed as "passed". The console output for any stage can also be viewed on this page.


## The gitlab-ci.yml File

This project's [`.gitlab-ci.yml`](./.gitlab-ci.yml) file provides a simple example of how to set up automatic building and testing for a generic C++ project (in this case, a simple doubly linked list implementation).
Some important parts of this project's '.gitlab-ci.yml' file are discussed below:
```
image: gcc
```
* This line pulls from a shared GitLab Runner which runs the official GCC Docker image (which is based on the Debian operating system).
* This environment setup is used to execute each stage's series of jobs that are outlined in the rest of the `.gitlab-ci.yml` file.

*--------------------*


```
stages:
  - build
  - test
```
* The `stages:` identifier allows for the stage names to be defined and their order of execution to be explicitly stated. The build stage will occur before the test stage.

*--------------------*


```
before_script:
  - apt update && apt -y install make autoconf
```
* The `before_script` tag is used to preface a series of commands that are meant to be executed before each stage is initiated. In this repository's YAML file, stages are executed in separate environments (GCC images on different virtual machines).
* `apt update && apt -y install make autoconf` ensures that the current image has the up-to-date package necessary to run makefiles when compiling/linking this project's C++ files.

*--------------------*


```
build:
  stage: build
  script:
     - make clean
     - make

  artifacts:
    paths:
      - CI-Test
```
* The `build:` line starts the definition for a job called _build_. All instructions that should be included in the job should be indented at least one extra level.
* `stage: build` declares that this job should occur in the stage named _build_. (Note: the job name is identical to the stage name)
* `script:` indicates that the subsequent indented lines should be executed as if they are contained in a shell script. `make clean` will clean up old object files (possibly built on a different machine), and then `make` will recompile everything according to the project's [makefile](./makefile).
* The `artifacts:` identifier is used to specify files that should be saved from the build and uploaded to GitLab. The artifacts that are saved from a given build can be directly downloaded from the project's `/pipelines` page. Artifacts are also accessible between different builds in the same CI pipeline. In this case, the `CI-Test` executable is saved from the build stage. Later on, the test stage will execute in an entirely different image, but it will still have access to this specific executable without having to recompile the C++ files within the project.

*--------------------*


```
# run tests using the binary built in the build stage
test:
  stage: test
  script:
    - ./CI-Test
```
* The first two lines define a job called _test_ that is executed within a stage of the same name. A new virtual machine is spun up from the same GCC Docker image used in the build stage.
* The `script:` here simply executes the given `CI-Test` executable, which is a testing file.

*--------------------*


## The Catch Test Framework

Catch is a header-only test framework designed for C++. In order to use this framework,
the [catch.hpp](./catch.hpp) file must to be included in the test file and linked with the
implementation C++ files being tested (which is done in our project's [makefile](./makefile).
Optionally, you can include a define statement that ensures that Catch will generate a main
that runs all of the test cases. An example header for the test file is the following:
```
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
```


### Using the Catch Test Framework

To write a test case in Catch, the macro `TEST\_CASE` is used.
Within a test case, different sections of tests can be created using the macro
`SECTION`. However, creating these sections is not required in order to execute
a test case. To enforce a test, `REQUIRE` or `CHECK` can be used. `REQUIRE` evaluates
the given expression and stops the tests if the assertion fails. `CHECK` also
evaluates the given expression, but continues running the remaining tests if the
assertion fails. If an assertion fails with either `REQUIRE` or `CHECK`, the program
shows you where the test failed and what values caused the test to fail. A very
simple test program using this framework can be found below:

```
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

int Add(int a, int b){
    return a + b;
}

TEST_CASE("Arithmetic operations", "[add]"){

    SECTION("Adding two integers"){

        REQUIRE( Add(9, 6) == 15 );

    }

}

```

For further details about this testing framework, go to the [Catch](https://github.com/philsquared/Catch/blob/master/docs/tutorial.md#test-cases-and-sections) documentation.
