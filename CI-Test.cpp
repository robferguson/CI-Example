#define CATCH_CONFIG_MAIN   
#include "catch.hpp"
#include "dlist.h"
#include <iostream>
#include <cstdlib>

Dlist *List = new Dlist();
Dnode *Node;

TEST_CASE("Inserting into a Dlist", "[dlist]" ){

    SECTION("Inserting the first element to the back of the list."){
        List->Push_Back("25");
        CHECK (List->Size() == 1);
        CHECK ((List->Begin())->s == "25");
        CHECK ((List->Begin())->flink == List->End());
        CHECK ((List->Begin())->blink == List->End());
    }
   
    SECTION("Inserting to the front of the list."){
        Node = List->Begin();
        List->Push_Front("1000");
        CHECK (List->Size() == 2);
        CHECK ((List->Begin())->s == "1000");
        CHECK ((List->Begin())->flink == Node);
        CHECK ((List->Begin())->blink == List->End());

    }

    SECTION("Testing Insert Before function."){
        Node = List->Rbegin();
        List->Insert_Before("400", Node);
        CHECK (List->Size() == 3);
        CHECK ((List->Begin())->flink->s == "400");
        CHECK ((List->End())->blink->blink->s == "400");
        
    }

}


TEST_CASE("Removing from a Dlist.", "[dlist]"){
    

    SECTION("Deleting an element using Pop_Front."){
        Node = List->Rbegin();
        List->Pop_Front();
        CHECK (List->Size() == 2);
        CHECK ((List->Begin())->s == "400");
        CHECK ((List->Begin())->flink == Node);
        CHECK ((List->Begin())->blink == List->End());
    }

    SECTION("Deleting an element using Pop_Back."){
        Node = List->Rbegin();
        List->Pop_Back();
        CHECK (List->Size() == 1);
        CHECK ((List->Begin())->s == "400");
        CHECK ((List->Begin())->flink == List->End());
        CHECK ((List->Begin())->blink == List->End());
    }

}
