#include <iostream>
#include <cstdlib>
#include "dlist.h"

using namespace std;

/*
   Robby Ferguson
   Cs140
   Lab8 - dlist.cpp

   dlist.cpp implements a Doubly-Linked List data structure. This allows for forward
   or reversed traversal of the list, insertion of arbitrary nodes anywhere in the
   list, and for deletion of arbitrary nodes from anywhere in the list. The class
   methods implemented allow for determining if a list is empty, determining the 
   size of the list, pushing onto/popping off the front, pushing onto/popping off
   the back, returning a pointer to nodes located at either the beginning, the end,
   the rbeginning, or the rend, inserting a new node before a given node, inserting
   a new node after a given node, and erasing any node. The list reroutes pointers
   as necessary to accomodate deletions and insertions anywhere in the list.
   */


//constructor
Dlist::Dlist(){
  //create the sentinel node
  sentinel = new Dnode;

  //initialize its flink and blink to point to itself
  sentinel->flink = sentinel;
  sentinel->blink = sentinel;

  //initialize size to 0
  size = 0;
}


//destructor
Dlist::~Dlist(){
  Dnode* pointer;                       //Dnode pointer to use for loop iterations;
  Dnode* next;                          //pointer to next element after one currently pointed to

  //iterate through list and delete each Dnode element
  //with each iteration, pointer gets reassigned to point to what is pointed to by next
  for(pointer = sentinel->flink; pointer != sentinel; pointer = next){
    //assign the next pointer to the next element in the list
    next = pointer->flink;

    //delete the current element being pointed to
    delete pointer;
  }

  //now delete the sentinel node
  delete sentinel;
  sentinel = NULL;
}


//return 1 if the list is currently empty
int Dlist::Empty(){
  //if the size member of the Dlist class is 0, return 1;
  if(Size() == 0) return 1;
  else return 0;
}


//return the size of the list
int Dlist::Size(){
  return size;
}


//push onto the front of the list
void Dlist::Push_Front(string s){
  //insert after the sentinel node to add an element to the front
  Insert_After(s, sentinel);
}


//push onto the back of the list
void Dlist::Push_Back(string s){
  //insert before the sentinel node to add an element to the back
  Insert_Before(s, sentinel);
}


//delete the first element of the list
string Dlist::Pop_Front(){
  string node_string = sentinel->flink->s;              //grab the string for the first element

  //to pop front, erase the element directly after the sentinel node
  Erase(sentinel->flink);

  return node_string;
}


//delete the last element of the list
string Dlist::Pop_Back(){
  string node_string = sentinel->blink->s;              //grab the string for the last element

  //to pop back, erase the element directly before the sentinel node
  Erase(sentinel->blink);

  return node_string;
}


//return a pointer to the first node in the list (for list iterations)
Dnode* Dlist::Begin(){
  return sentinel->flink;
}


//return a pointer to the sentinel node (for list iterations)
Dnode* Dlist::End(){
  return sentinel;
}


//return a pointer to the last element in the lsit (for list iterations)
Dnode* Dlist::Rbegin(){
  return sentinel->blink;
}


//return a pointer to the sentinel node (for list iterations)
Dnode* Dlist::Rend(){
  return sentinel;
}


//insert a new node before the given node
void Dlist:: Insert_Before(string s, Dnode *n){
  Dnode* prev = n->blink;                       //pointer to element before one pointed to by n
  Dnode* newnode;                               //pointer to the new node being created

  //create a new Dnode for the new string
  newnode = new Dnode;

  //set its parameters for inserting before the node pointed to by n
  newnode->s = s;
  newnode->flink = n;
  newnode->blink = prev;

  //set the flink and blink pointers of surrounding nodes to include the new node
  n->blink = newnode;
  prev->flink = newnode;

  //increment the size of the doubly-linked list
  size++;
}


//insert a new node after the given node
void Dlist:: Insert_After(string s, Dnode *n){
  Dnode* next = n->flink;                       //pointer to element after one pointed to by n
  Dnode* newnode;                               //pointer to the new node being created

  //create a new Dnode for the new string
  newnode = new Dnode;

  //set its parameters for inserting after the node pointed to by n
  newnode->s = s;
  newnode->blink = n;
  newnode->flink = next;

  //set the flink and blink pointers of surrounding nodes to include the new node
  n->flink = newnode;
  next->blink = newnode;

  //increment the size of the doubly-linked list
  size++;
}


//erase a given node
void Dlist::Erase(Dnode *n){
  Dnode* prev = n->blink;                       //pointer to element before one pointed to by n
  Dnode* next = n->flink;                       //pointer to element after one pointed to by n

  //do not erase the sentinel node
  if(n != sentinel){
    //connect objects pointed to by prev and next pointers
    //reroute around n, rather than through it
    prev->flink = next;
    next->blink = prev;

    //delete the object pointed to by n
    delete n;

    //decrement the size of the doubly-linked list
    size--;
  }
}


